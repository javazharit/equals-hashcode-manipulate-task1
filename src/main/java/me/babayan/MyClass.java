package me.babayan;

import java.util.HashSet;
import java.util.Set;

public class MyClass {
    static int count = 0;

    @Override
    public boolean equals(Object obj) {
        return ++count > 1;
    }

    @Override
    public int hashCode() {
        return 118;
    }

    public static void main(String[] args) {
        MyClass ins1 = new MyClass();
        MyClass ins2 = new MyClass();

        Set<MyClass> set = new HashSet<MyClass>();

        set.add(ins1);
        set.add(ins2);
        System.out.println("size = " + set.size());

        set.clear();

        set.add(ins2);
        set.add(ins1);
        System.out.println("size = " + set.size());
    }
}
